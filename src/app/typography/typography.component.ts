import { Component, OnInit } from '@angular/core';

declare interface TableData {
  headerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'app-typography',
  templateUrl: './typography.component.html',
  styleUrls: ['./typography.component.css']
})
export class TypographyComponent implements OnInit {
  public tableData1: TableData;
  public tableData2: TableData;

constructor() { }

ngOnInit() {
    this.tableData1 = {
      headerRow: [ 'Fecha', 'Total' ],
      dataRows: [
          [ '00/00/2022','$36,738' ],
          [ '00/00/2022', '$23,789'],
          [ '00/00/2022', '$56,142' ],
          [ '00/00/2022', '$38,735' ],
          [ '00/00/2022', '$63,542' ],
          [ '00/00/2022', '$78,615' ]
        ]
    };
    this.tableData2 = {
        headerRow: [ 'Fecha', 'Total' ],
        dataRows: [
            [ '00/00/2022','$36,738' ],
            [ '00/00/2022', '$23,789'],
            [ '00/00/2022', '$56,142' ],
            [ '00/00/2022', '$38,735' ],
            [ '00/00/2022', '$63,542' ],
            [ '00/00/2022', '$78,615' ]
        ]
    };
}

}

