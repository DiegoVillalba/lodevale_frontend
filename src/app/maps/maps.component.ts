import { Component, OnInit } from '@angular/core';

declare interface TableData {
  headerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})

export class MapsComponent implements OnInit {
  public tableData1: TableData;
  public tableData2: TableData;

constructor() { }

ngOnInit() {
    this.tableData1 = {
        headerRow: [ 'Cantidad', 'Nombre del Producto', 'Precio de Compra', 'Fecha de Recepcion', 'Sub-Nombre del Proveedor', 'Total'],
        dataRows: [
            ['1', 'Dakota Rice', '3000', '2022/03/14', 'Diego', '$36,738'],
            ['20', 'Minerva Hooper', '5000', '2022/02/16', 'Alan', '$23,789'],
            
        ]
    };
    this.tableData2 = {
        headerRow: [ 'ID', 'Name',  'Salary', 'Country', 'City' ],
        dataRows: [
            ['1', 'Dakota Rice','$36,738', 'Niger', 'Oud-Turnhout' ],
            ['2', 'Minerva Hooper', '$23,789', 'Curaçao', 'Sinaai-Waas'],
            ['3', 'Sage Rodriguez', '$56,142', 'Netherlands', 'Baileux' ],
            ['4', 'Philip Chaney', '$38,735', 'Korea, South', 'Overland Park' ],
            ['5', 'Doris Greene', '$63,542', 'Malawi', 'Feldkirchen in Kärnten', ],
            ['6', 'Mason Porter', '$78,615', 'Chile', 'Gloucester' ]
        ]
    };
}

}

